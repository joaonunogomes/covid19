build:
	docker-compose build

build_all: build
	docker-compose run --rm composer install
	docker-compose run --rm npm ci
	docker-compose run --rm npm prod

build_all_dev: build
	docker-compose run --rm composer install
	docker-compose run --rm npm ci
	docker-compose run --rm npm run dev

build_auth:
	docker-compose run --rm artisan passport:keys
	# docker-compose run --rm artisan passport:client
	# docker-compose run --rm artisan passport:client --password

run:
	docker-compose up -d


import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getSuspect } from "../suspect-case-page/services";
import { IdentificationPicture } from "../suspect-case-page/identification-picture";
import { ClinicalPicture } from "../suspect-case-page/clinical-picture";
import { TravelPicture } from "../suspect-case-page/travel-picture";
import { getClinicalCaseConfig } from "../suspect-case-page/view-config-service";
import { SymptomMap, Symptom, Suspect } from "../suspect-case-page/types";
import { NullableCheckbox } from "../nullable-checkbox";

function PersonPageContainer(): JSX.Element {
    const { id } = useParams();
    const [symptomsConfig, setSymptomConfig] = useState<SymptomMap>();
    const [symptoms, setSymptoms] = useState<Symptom[][]>([]);
    const [suspect, setSuspect] = useState<Suspect>();

    useEffect(() => {
        (async (): Promise<void> => {
            const symptomsMapConfig = await getClinicalCaseConfig();

            setSymptomConfig(symptomsMapConfig);
        })();

        if (!id) {
            return;
        }

        (async (): Promise<void> => {
            const fetchedSuspect = await getSuspect(id);

            setSuspect(fetchedSuspect);
        })();
    }, []);

    useEffect(() => {
        if (!suspect || !symptomsConfig) {
            return;
        }

        const suspectSymptoms = Object.values(symptomsConfig)
            .map(symptom => ({ ...symptom, value: suspect[symptom.id] }))
            .reduce<{
                [id: string]: Symptom[];
            }>(
                (acc, curr) => ({
                    ...acc,
                    [curr.group]: [...(acc[curr.group] ?? []), curr]
                }),
                {}
            );

        setSymptoms(Object.values(suspectSymptoms));
    }, [suspect, symptomsConfig]);

    if (!suspect) {
        return <></>;
    }

    const hasTraveledToForeign = !!(
        suspect.trip_departure ||
        suspect.trip_return ||
        suspect.recent_trips
    );

    return (
        <>
            <NullableCheckbox label="Positivo" value={suspect.is_positive} />
            <IdentificationPicture
                disabled
                name={suspect.name}
                birthDate={suspect.birth_date}
                gender={suspect.gender ?? ""}
                nationality={suspect.nationality ?? ""}
                profession={suspect.profession}
                email={suspect.email}
                residence={suspect.address}
                outsideCountryResidence={
                    suspect.outside_country_residence ?? ""
                }
                location={suspect.location ?? ""}
                contact={suspect.contact}
                friendContact={suspect.friend_contact}
                registration={suspect.registration}
                documentNumber={suspect.id_number}
                documentType={suspect.id_type}
            ></IdentificationPicture>
            <ClinicalPicture
                disabled
                symptomGroups={symptoms}
            ></ClinicalPicture>

            <TravelPicture
                disabled
                traveledToForeign={hasTraveledToForeign}
                foreignCountry={suspect.recent_trips ?? ""}
                startTravelDate={new Date(suspect.trip_departure)}
                returnTravelDate={new Date(suspect.trip_return)}
                contactWithInfected={suspect.contact_confirmed_case ?? "NO"}
                confirmedCaseName={suspect.contact_confirmed_case_name ?? ""}
                confirmedCaseContactDate={
                    new Date(suspect.contact_confirmed_case_date)
                }
                isHealthProfessional={suspect.is_health_profissional ?? false}
            ></TravelPicture>
        </>
    );
}

export { PersonPageContainer };

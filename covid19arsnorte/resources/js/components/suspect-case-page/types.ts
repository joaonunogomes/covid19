import { Entry, DOCUMENT_TYPES } from "../../types";

export interface CheckSymptom {
    id: string;
    name: string;
    type: SYMPTOM_TYPE.CHECK;
    value: boolean;
    group: number;
}

export interface FreeTextSymptom {
    id: string;
    name: string;
    type: SYMPTOM_TYPE.FREE_TEXT;
    value: string;
    group: number;
}

export enum SYMPTOM_TYPE {
    CHECK,
    FREE_TEXT
}

export type Symptom = CheckSymptom | FreeTextSymptom;

export interface SymptomMap {
    [id: string]: Symptom;
}

export interface Suspect {
    id?: string;
    id_number: string;
    id_type: DOCUMENT_TYPES;
    name: string;
    birth_date: Date;
    gender: string;
    contact: string;
    address: string;
    registration: string;
    date_first_symptoms: Date;
    fever: boolean;
    cough: boolean;
    chills: boolean;
    otophagia: boolean;
    coryza: boolean;
    headaches: boolean;
    myalgia: boolean;
    dyspnoea: boolean;
    abdominalPain: boolean;
    vomits: boolean;
    diarrhea: boolean;
    otherSymptoms: string;
    outside_country_residence: string;
    location: string;
    friend_contact: string;
    recent_trips: string;
    trip_departure: string;
    trip_return: string;
    trip_return_portugal: Date;
    contact_confirmed_case: string;
    contact_confirmed_case_name: string;
    contact_confirmed_case_date: Date;
    is_health_profissional: boolean;
    is_approved: boolean;
    nationality: string;
    profession: string;
    email: string;
    originalEntry?: Entry;
    is_positive: boolean;
}

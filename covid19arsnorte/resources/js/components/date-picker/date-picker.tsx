import React from "react";
import "date-fns";
import { KeyboardDatePicker } from "@material-ui/pickers";

interface DatePickerProps {
    label: string;
    value: Date;
    disabled?: boolean;
    format?: string;
    required?: boolean;
    minDate?: Date;
    minDateMessage?: string;
    maxDate?: Date;
    maxDateMessage?: string;
    autoOk?: boolean;
    disableFuture?: boolean;
    onDateChange: (date: Date) => void;
}

function DatePicker({
    label,
    value,
    disabled,
    required,
    minDate,
    minDateMessage,
    maxDate,
    maxDateMessage,
    format = "dd/MM/yyyy",
    onDateChange,
    autoOk,
    disableFuture
}: DatePickerProps): JSX.Element {
    const handleDateChange = onDateChange;

    return (
        <KeyboardDatePicker
            required={required}
            disabled={disabled}
            label={label}
            style={{ display: "flex" }}
            format={format}
            value={value}
            minDate={minDate}
            minDateMessage={minDateMessage}
            maxDate={maxDate}
            maxDateMessage={maxDateMessage}
            onChange={handleDateChange}
            autoOk={autoOk}
            disableFuture={disableFuture}
            KeyboardButtonProps={{
                "aria-label": "change date"
            }}
        />
    );
}

export { DatePicker };

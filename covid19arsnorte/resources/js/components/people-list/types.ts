import { DOCUMENT_TYPES, Entry } from "../../types";

export interface Person {
    key: string;
    id: string;
    documentType: DOCUMENT_TYPES;
    name: string;
    hasArrived: boolean | null;
    hasContractedDisease: boolean | null;
    registration: string;
    createdDate: Date;
    updatedDate: Date;
    originalEntry: Entry; // Hammer time
}

export interface PeopleMap {
    [idPerDocumentType: string]: Person;
}

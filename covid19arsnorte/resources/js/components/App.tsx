import React from "react";
import * as ReactDom from "react-dom";
import DateFnsUtils from "@date-io/date-fns";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { Container, LinearProgress } from "@material-ui/core";

import { GlobalStateProvider, useGlobalState } from "../context";
import { SuspectedCaseContainer } from "./suspect-case-page";
import { PeopleListContainer } from "./people-list";
import { GLOBAL_ACTION_TYPE, GlobalState, GlobalAction } from "../types";
import { PersonPageContainer } from "./person-page";

function reducer(state: GlobalState, action: GlobalAction): GlobalState {
    switch (action.type) {
        case GLOBAL_ACTION_TYPE.START_LOADING:
            return {
                ...state,
                isLoadingCounter: state.isLoadingCounter + 1
            };
        case GLOBAL_ACTION_TYPE.STOP_LOADING:
            return {
                ...state,
                isLoadingCounter: state.isLoadingCounter - 1
            };
        case GLOBAL_ACTION_TYPE.UPDATE_PEOPLE_MAP:
            return {
                ...state,
                peopleMap: {
                    ...state.peopleMap,
                    ...action.payload
                }
            };
        case GLOBAL_ACTION_TYPE.UPDATE_PERSON:
            return {
                ...state,
                peopleMap: {
                    ...state.peopleMap,
                    [action.payload.key]: action.payload
                }
            };
        default:
            return state;
    }
}

function GlobalizedApp(): JSX.Element {
    const [{ isLoadingCounter }] = useGlobalState();

    return (
        <>
            {isLoadingCounter > 0 && (
                <LinearProgress
                    style={{ position: "sticky", top: 0, width: "100%" }}
                ></LinearProgress>
            )}
            <Container maxWidth="lg">
                <Router>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Switch>
                            <Route path="/form-beta">
                                <SuspectedCaseContainer></SuspectedCaseContainer>
                            </Route>
                            <Route path="/people-list">
                                <PeopleListContainer></PeopleListContainer>
                            </Route>
                            <Route path="/person/:id">
                                <PersonPageContainer></PersonPageContainer>
                            </Route>
                            <Route path="/something">World</Route>
                            <Route path="/404">404!</Route>
                            <Redirect exact path="/" to="/form-beta"></Redirect>
                            <Redirect to="/404"></Redirect>
                        </Switch>
                    </MuiPickersUtilsProvider>
                </Router>
            </Container>
        </>
    );
}

function App(): JSX.Element {
    return (
        <GlobalStateProvider reducer={reducer}>
            <GlobalizedApp></GlobalizedApp>
        </GlobalStateProvider>
    );
}

export default App;

if (document.getElementById("app")) {
    ReactDom.render(<App />, document.getElementById("app"));
}

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ mix('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />

    <!-- Styles -->
    @yield('head')
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <script>
        window._env = { API_ENDPOINT: "{{env('API_ENDPOINT', 'laravel')}}"}
    </script>
    <div>
        <nav class="navbar navbar-sm">
            <div class="container">
                @auth
                <a class="navbar-brand" href="{{ Auth::user()->isLabUser() ? url('/people-list') : url('/form-beta') }}">
                    ARS Norte
                </a>
                @endauth

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto flex-row">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item" style="margin-right: 40px">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @else
                    <div style="display: flex;">
                        @if(!Auth::user()->isLabUser())
                        <div>
                            <a class="dropdown-item" href="/form-beta">
                                Formulário
                            </a>
                        </div>
                        @endif
                        <div>
                            <a class="dropdown-item" href="/people-list">
                                Lista
                            </a>
                        </div>
                        @if(Auth::user()->isAdmin())
                        <div>
                            <a class="dropdown-item" href="{{ route('register') }}">
                               Adicionar Utilizador
                            </a>
                        </div>
                        <div>
                            <a class="dropdown-item" href="{{ route('manage-roles') }}">
                                Gerir Permissões
                            </a>
                        </div>
                        @endif
                        <div>
                            <span class="dropdown-item">
                                {{ Auth::user()->name }}
                            </span>
                        </div>
                        <div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                    @endguest
                </ul>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>

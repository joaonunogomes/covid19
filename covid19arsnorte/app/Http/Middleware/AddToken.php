<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class AddToken
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = isset($_COOKIE["jwt_token"])?$_COOKIE["jwt_token"]:"";
        //$request['token'] = $token;//this is working
        $request->headers->set("Authorization", "Bearer $token");//this is working
        $response = $next($request);
        //$response->header('header name', 'header value');
        return $response;
    }
}

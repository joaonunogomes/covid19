<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function manageRoles() {
        $users = User::all();

        return view('user-manage')->with('users', $users);
    }

    public function changeRole(Request $request) {
        $userID = $request->get('id');
        $type = $request->get('type');

        if(!$userID || !$type) {
            return abort(400);
        }

        $user = User::find($userID);
        $user->user_type = $type;
        $user->save();

        return view('user-manage')->with('users', User::all())->with('message', $user->name . ' passou com sucesso a ' . $user->userType());
    }
}
